import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv) #initializing moveit_commander

rospy.init_node('move_group_drawing_j_b_w',
                anonymous=True) #initializing rospy node

robot = moveit_commander.RobotCommander() #initializing RobotCommander object (outer-level interface to the robot 

scene = moveit_commander.PlanningSceneInterface() #initializing PlanningSceneInterface object (interface to the world surrounding robot)

group_name = "manipulator"
group = moveit_commander.MoveGroupCommander(group_name) #initializing MoveGroupCommander (interface to a group of joints) 

display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                               moveit_msgs.msg.DisplayTrajectory,
                                               queue_size=20) #creating ROS publisher used to display trajectories in RViz

def set_joint_goal(group, q1, q2, q3, q4, q5, q6): #defining function for setting initial joint values
	joint_goal = group.get_current_joint_values() #getting current joint values 
	#changing values of joint values
	joint_goal[0] = q1
	joint_goal[1] = q2
	joint_goal[2] = q3
	joint_goal[3] = q4
	joint_goal[4] = q5
	joint_goal[5] = q6
	group.go(joint_goal, wait=True) #making robot achieve these joint values
	group.stop() #calling stop to ensure that there is no residual movement


def set_pose_goal(x, y, z): #defining function for setting pose goals for the letters to start at
	pose_goal = geometry_msgs.msg.Pose() #getting pose
	#setting orientation and position values to plan pose 
	pose_goal.orientation.w = 1.0
	pose_goal.position.x = x
	pose_goal.position.y = y
	pose_goal.position.z = z
	group.set_pose_target(pose_goal) #setting pose target to set orientation and position
	plan = group.go(wait=True) #sending robot to this desired pose
	#calling `stop()` ensures that there is no residual movement
	group.stop() #calling stop to ensure there is no residual movement

#initializing joint orientations
set_joint_goal(group, -2, -2, -0.5, -0.6, 0.5, 0)

set_pose_goal(0.4, 0.4, 0.8) #setting initial pose goal of J as x=0.4. y=0.4, z=0.8 

print("initial pose goal:", group.get_current_joint_values()) #printing joint values at initial J for plugging into A and J matrices 

group.clear_pose_targets() #clearing target after planning with poses


#creating cartesian path for drawing J
waypoints = [] #list of waypoints for end effector to go through
scale=1 #setting scale to 1 meter
wpose = group.get_current_pose().pose
wpose.position.z -= scale * 0.4  # move downwards 0.4 m in z direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.x += scale * 0.2 #move left 0.2 m in x direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.z += scale * 0.1 #move upwards 0.1 m  in x direction
waypoints.append(copy.deepcopy(wpose))

#setting up cartesian path plan
(plan, fraction) = group.compute_cartesian_path(
                                   waypoints,   # waypoints to follow
                                   0.01,        # eef step; results in Cartesian path being interpolated at a resolution of 1 cm
                                   0.0)         # jump_threshold; set to 0 to disable


group.execute(plan, wait=True) #instructing robot to follow cartesian path plan just specified
print("end of J:",group.get_current_joint_values()) #printing joint values that will be plugged into A and J




#drawing B
set_pose_goal(0.4, 0.4, 0.4) #setting initial pose goal of B (bottom left corner) as x=0.4, y=0.4, z=0.4
group.clear_pose_targets()

#adding waypoints for the end effector to pass through, creating plan with waypoints, and having robot execute the path
waypoints = []
scale=1
wpose = group.get_current_pose().pose
wpose.position.z += scale * 0.4  # move upwards 0.4 in z direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.x -= scale * 0.2 #move right 0.2 in x direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.z -= scale * 0.2 #move down 0.2 in z direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.x += scale * 0.2 #move left 0.2 in x direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.x -= scale * 0.2 #move right 0.2 in x direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.z -= scale * 0.2 #move down 0.2 in z direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.x += scale * 0.2 #move left 0.2 in x direction
waypoints.append(copy.deepcopy(wpose))


(plan, fraction) = group.compute_cartesian_path(
                                   waypoints,   # waypoints to follow
                                   0.01,        # eef_step
                                   0.0)         # jump_threshold

group.execute(plan, wait=True)





#drawing W 
set_pose_goal(0.4, 0.4, 0.8) #setting initial pose goal of W (top left corner) to x=0.4, y=0.4, z=0.8
group.clear_pose_targets() #clearing pose targets after setting pose goal

#adding waypoints for the end effector to pass through, creating plan with waypoints, and having robot execute the path
waypoints = []
scale=1
wpose = group.get_current_pose().pose
wpose.position.z -= scale * 0.4  # move downwards 0.4 m in z direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.x -= scale * 0.1 #move right  0.3 m in x direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.z += scale * 0.2 #move up 0.2 m in z direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.x -= scale * 0.1 #move right 0.1 m in x direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.z -= scale * 0.2 #move down 0.2 m in z direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.x -= scale * 0.1 #move right 0.1 m in x direction
waypoints.append(copy.deepcopy(wpose))
wpose.position.z += scale * 0.4 #move up 0.4 m in z direction
waypoints.append(copy.deepcopy(wpose))


(plan, fraction) = group.compute_cartesian_path(
                                   waypoints,   # waypoints to follow
                                   0.01,        # eef_step
                                   0.0)         # jump_threshold

group.execute(plan, wait=True)

#print("bottom left corner of W", group.get_current_joint_values()) #used to find middle of W (commented out way points moving right 0.1 m and up 0.4 m at end of W) 

