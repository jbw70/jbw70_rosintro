rosservice call turtle1/teleport_absolute 1.0 5.0 0.0

rosservice call clear

rosservice call kill turtle2 #if turtle2 is still there from the last run

rosservice call turtle1/set_pen 0 0 100 4 off


rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
         -- '[0.0,-5.0, 0.0]' '[0.0, 0.0, 3.12]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
         -- '[0.0, -3.5, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /spawn 5.5 5.0 0.0 turtle2

rosservice call turtle2/set_pen 100 200 100 4 off

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
         -- '[3.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
         -- '[0.0, 8.5, 0.0]' '[0.0, 0.0, 5.6]'


